#!/usr/bin/env python
# -*- coding: utf-8 -*-

# plateau: List[List[nat]]
# liste de listes (lignes du plateau) d'entiers correspondant aux contenus des cases du plateau de jeu

# coup:[nat nat]
# Numero de ligne et numero de colonne de la case correspondante a un coup d'un joueur

# Jeu
# jeu:[plateau nat List[coup] List[coup] List[nat nat]]
# Structure de jeu comportant :
#           - le plateau de jeu (0)
#           - Le joueur a qui c'est le tour de jouer (1 ou 2) (1)
#           - La liste des coups possibles pour le joueur a qui c'est le tour de jouer (2)
#           - La liste des coups joues jusqu'a present dans le jeu (3)
#           - Une paire de scores correspondant au score du joueur 1 et du score du joueur 2 (4)

from copy import deepcopy
game=None #Contient le module du jeu specifique: awele ou othello
joueur1=None #Contient le module du joueur 1
joueur2=None #Contient le module du joueur 2


#Fonctions minimales 

def getCopieJeu(jeu):
    """ jeu->jeu
        Retourne une copie du jeu passe en parametre
        Quand on copie un jeu on en calcule forcement les coups valides avant
    """
    if jeu[2] is None:
        jeu[2] = game.listeCoupsValides(jeu)
    return deepcopy(jeu)


def finJeu(jeu):
    """ jeu -> bool
        Retourne vrai si c'est la fin du jeu
    """
    if not getCoupsValides(jeu):
        return True
    return game.finJeu(jeu)


def saisieCoup(jeu):
    """ jeu -> coup
        Retourne un coup a jouer
        On suppose que la fonction n'est appelee que si il y a au moins un coup valide possible
        et qu'elle retourne obligatoirement un coup valide
    """
    j_courant = jeu[1]
    #print("C'est au tour du joueur {} !".format(j_courant))
    if j_courant == 1:
        coup = joueur1.saisieCoup(jeu)
    else:
        coup = joueur2.saisieCoup(jeu)
    return coup


def getCoupsValides(jeu):
    """ jeu  -> List[coup]
        Retourne la liste des coups valides dans le jeu passe en parametre
        Si None, alors on met � jour la liste des coups valides
    """
    if jeu[2] is None: 
        jeu[2] = game.listeCoupsValides(jeu).copy()
    return jeu[2]

def coupValide(jeu,coup):
    """jeu*coup->bool
        Retourne vrai si le coup appartient a la liste de coups valides du jeu
   """
    return coup in jeu[2]

def joueCoup(jeu,coup):
    """jeu*coup->void
        Joue un coup a l'aide de la fonction joueCoup defini dans le module game
        Hypothese:le coup est valide
        Met tous les champs de jeu à jour (sauf coups valides qui est fixée à None)
    """
    game.joueCoup(jeu,coup)
    jeu[3].append(coup)
    jeu[2] = None
    changeJoueur(jeu)



def initialiseJeu():
    """ void -> jeu
        Initialise le jeu (nouveau plateau, liste des coups joues vide, liste des coups valides None, scores a 0 et joueur = 1)
    """
    
    plateau = game.initialisePlateau()
    score = game.initScore()
    joueur = 1
    liste_coups_possible = None
    liste_coups_joues = list()
    return [ plateau, joueur, liste_coups_possible, liste_coups_joues, score]
    

def getGagnant(jeu):
    """jeu->nat
    Retourne le numero du joueur gagnant apres avoir finalise la partie. Retourne 0 si match nul
    """
    return game.getGagnant(jeu)


def affiche(jeu):
    """ jeu->void
        Affiche l'etat du jeu de la maniere suivante :
                 Coup joue = <dernier coup>
                 Scores = <score 1>, <score 2>
                 Plateau :

                         |       0     |     1       |      2     |      ...
                    ------------------------------------------------
                      0  | <Case 0,0>  | <Case 0,1>  | <Case 0,2> |      ...
                    ------------------------------------------------
                      1  | <Case 1,0>  | <Case 1,1>  | <Case 1,2> |      ...
                    ------------------------------------------------
                    ...       ...          ...            ...
                 Joueur <joueur>, a vous de jouer
                    
         Hypothese : le contenu de chaque case ne depasse pas 5 caracteres
    """
    taille_liste_coups_joues = len(jeu[3])
    if taille_liste_coups_joues != 0:
        print("Coup joue = {}".format(jeu[3][taille_liste_coups_joues - 1]))

    print("Scores = {}, {}".format(jeu[4][0], jeu[4][1]))
    print("Plateau : ")
    
    nb_lignes = len(jeu[0])
    nb_colonnes = len(jeu[0][0])
    print("\t|",end="")
    for i in range(nb_colonnes) :
        print("\t{}\t|".format(i),end="")
    print("")
    print("-----",end="")
    print("--------"*nb_colonnes)
    for w in range(nb_lignes) :
        print("{}\t|".format(w),end="")
        for k in range(nb_colonnes) :
            print("\t{}\t|".format(jeu[0][w][k]), end="")
        print("")
        print("-----", end="")
        print("--------" * nb_colonnes)


# Fonctions utiles

def getPlateau(jeu):
    """ jeu  -> plateau
        Retourne le plateau du jeu passe en parametre
    """
    return jeu[0]

def getCoupsJoues(jeu):
    """ jeu  -> List[coup]
        Retourne la liste des coups joues dans le jeu passe en parametre
    """
    return jeu[3]



def getScores(jeu):
    """ jeu  -> Pair[nat nat]
        Retourne les scores du jeu passe en parametre
    """
    return jeu[4]

def getJoueur(jeu):
    """ jeu  -> nat
        Retourne le joueur a qui c'est le tour de jouer dans le jeu passe en parametre
    """
    return jeu[1]

def changeJoueur(jeu):
    """ jeu  -> void
        Change le joueur a qui c'est le tour de jouer dans le jeu passe en parametre (1 ou 2)
    """
    joueur = 0
    if jeu[1] == 1:
        jeu[1] = 2
    else:
        jeu[1] = 1

def getScore(jeu,joueur):
    """ jeu*nat->int
        Retourne le score du joueur
        Hypothese: le joueur est 1 ou 2
    """
    return jeu[4][joueur-1]

def getCaseVal(jeu, ligne, colonne):
    """ jeu*nat*nat -> nat
        Retourne le contenu de la case ligne,colonne du jeu
        Hypothese: les numeros de ligne et colonne appartiennent bien au plateau  : ligne<=getNbLignes(jeu) and colonne<=getNbColonnes(jeu)
    """
    return jeu[0][ligne][colonne]
