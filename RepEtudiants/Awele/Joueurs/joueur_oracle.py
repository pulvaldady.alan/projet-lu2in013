#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import game
import random
import math

l = [1.2, -0.026, 0.3, 0.2, 0.6]
moi = None
liste_coup_eval = None


def saisieCoup(jeu):
    """ jeu -> coup
        Retourne un coup a jouer
    """
    global moi
    moi = game.getJoueur(jeu)
    coup = decision(jeu)
    return coup


def decision(jeu):
    """ jeu -> coup
        Regarde quel coup est le meilleur puis le renvoie
    """
    global liste_coup_eval
    liste_coup_eval = list()
    liste_coup_max = list()
    alpha = -math.inf

    for coup in game.getCoupsValides(jeu):
        tab_coup_eval = [coup, estimation(jeu, coup, 4, alpha,  math.inf)]
        alpha = tab_coup_eval[1]
        liste_coup_eval.append(tab_coup_eval)

    # Max sur les evals puis on prend le coup associé

    eval_max = liste_coup_eval[0][1]
    liste_coup_max.append(liste_coup_eval[0][0])

    for i in range(1, len(liste_coup_eval)):
        if eval_max <= liste_coup_eval[i][1]:
            eval_max = liste_coup_eval[i][1]
            liste_coup_max = []
        if eval_max == liste_coup_eval[i][1]:
            liste_coup_max.append(liste_coup_eval[i][0])
    return liste_coup_max[random.randint(0, len(liste_coup_max) - 1)]


def estimation(jeu, coup, profondeur, alpha, beta):
    """ jeu * coup -> float
        Pour un coup donne, evalue la satisfaction obtenue
    """

    # Copier le jeu , jouer le coup, retourner l'application de la fonction eval sur le jeu modif

    # Copie du jeu
    copie_jeu = game.getCopieJeu(jeu)

    # Jouer le coup
    game.joueCoup(copie_jeu, coup)
    # print(game.finJeu(copie_jeu))
    if game.finJeu(copie_jeu):
        g = game.getGagnant(copie_jeu)
        if g == 0:
            return -100
        if g == game.getJoueur(copie_jeu):
            return 1000
        else:
            return -1000
    v = 0
    if profondeur == 1:
        return evaluation(copie_jeu)
    if game.getJoueur(copie_jeu) != moi: # Noeud MIN
        v = math.inf
        for c in game.getCoupsValides(copie_jeu):
            v = min(v, estimation(copie_jeu, c, profondeur - 1, alpha, beta))
            if v <= alpha:
                return v-1
            beta = min(beta, v)
    if game.getJoueur(copie_jeu) == moi: # Noeud MAX
        v = - math.inf
        for c in game.getCoupsValides(copie_jeu):
            v = max(v, estimation(copie_jeu, c, profondeur - 1, alpha, beta))
            if v >= beta:
                return v
            alpha = max(alpha, v)
    return v


def getScore(jeu):
    return [difference_score(jeu), cases_attaques(jeu), cases_012_consecutives(jeu),
            piege(jeu), garderGraine(jeu)]


def evaluation(jeu):
    return prodScal(l, getScore(jeu))


def prodScal(v1, v2):
    somme = 0
    for i in range(len(v1)):
        somme += v1[i] * v2[i]
    return somme


def difference_score(jeu):
    score = jeu[4]
    # print(jeu[4])
    j_courant = moi
    # print("Joueur :",j_courant,"Test")
    if j_courant == 1:
        j_adverse = 2
    else:
        j_adverse = 1
    # print(score[j_courant-1])
    return score[j_courant - 1] - score[j_adverse - 1]


def cases_012_consecutives(jeu):
    plateau = jeu[0]
    j_courant = moi
    cpt_012 = 0
    nb_012_max = 0
    for e in plateau[j_courant - 1]:
        if e == 0 or e == 1 or e == 2:
            cpt_012 += 1
        else:
            if cpt_012 > nb_012_max:
                nb_012_max = cpt_012
            cpt_012 = 0
    return nb_012_max


def garderGraine(jeu):
    plateau = jeu[0]
    j_courant = moi
    nb_graines = 0
    for e in plateau[j_courant - 1 ]:
        nb_graines += e
    return nb_graines


def piege(jeu):
    plateau = jeu[0]
    j_courant = moi
    if j_courant == 1:
        j_adverse = 2
    else:
        j_adverse = 1
    cpt_012 = 0
    nb_012_max = 0
    for e in plateau[j_adverse-1]:
        if e == 1 or e == 2:
            cpt_012 += 1
        else:
            if cpt_012 > nb_012_max:
                nb_012_max = cpt_012
            cpt_012 = 0
    return nb_012_max


def nbr_graine(jeu):
    plateau = jeu[0]
    nb_graines = 0

    for e in plateau[0]:
        nb_graines += e

    for e in plateau[1]:
        nb_graines += e

    return nb_graines


def cases_attaques(jeu):
    plateau = jeu[0]
    j_courant = moi
    nb_cases_att = 0

    for i in range(6):
        c = plateau[j_courant-1][i]
        if c >= 6 - i:
            nb_cases_att += 1

    return nb_cases_att
