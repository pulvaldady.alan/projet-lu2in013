#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import random
sys.path.append("../..")
import game
from datetime import datetime
def saisieCoup(jeu):
    """ jeu -> coup
        Retourne un coup a jouer
    """
    random.seed(datetime.now())
    l_coups_valides = game.getCoupsValides(jeu)
    indice_coup = random.randint(0, len(l_coups_valides) - 1)
    return l_coups_valides[indice_coup]
