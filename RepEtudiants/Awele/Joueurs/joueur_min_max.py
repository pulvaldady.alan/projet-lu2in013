#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import game
import random

moi = None

def saisieCoup(jeu):
    """ jeu -> coup
        Retourne un coup a jouer
    """
    global moi
    moi = game.getJoueur(jeu)
    coup = decision(jeu)
    return coup


def decision(jeu):
    """ jeu -> coup
        Regarde quel coup est le meillieur puis le renvoie
    """
    liste_coup_eval = list()
    liste_coup_max = list()
    for coup in game.getCoupsValides(jeu):
        tab_coup_eval = [coup, estimation(jeu, coup)]
        liste_coup_eval.append(tab_coup_eval)

    # Max sur les evals puis on prend le coup associé

    eval_max = liste_coup_eval[0][1]
    liste_coup_max.append(liste_coup_eval[0][0])

    for i in range(1, len(liste_coup_eval)):
        if eval_max <= liste_coup_eval[i][1]:
            eval_max = liste_coup_eval[i][1]
            liste_coup_max = []
        if eval_max == liste_coup_eval[i][1]:
            liste_coup_max.append(liste_coup_eval[i][0])
    return liste_coup_max[random.randint(0, len(liste_coup_max)-1)]


def estimation(jeu, coup, profondeur = 2):
    """ jeu * coup -> float
        Pour un coup donne, evalue la satisfaction obtenue
    """

    # Copier le jeu , jouer le coup, retourner l'application de la fonction eval sur le jeu modif

    # Copie du jeu
    copie_jeu = game.getCopieJeu(jeu)

    # Jouer le coup
    game.joueCoup(copie_jeu, coup)
    # print(game.finJeu(copie_jeu))
    if game.finJeu(copie_jeu):
        g = game.getGagnant(copie_jeu)
        if g == 0:
            return -100
        if g == game.getJoueur(copie_jeu):
            return 1000
        else:
            return -1000

    if profondeur == 1:
        return evaluation(copie_jeu)
    if game.getJoueur(copie_jeu) != moi:
        return min([estimation(copie_jeu, c, profondeur-1) for c in game.getCoupsValides(copie_jeu)])
    if game.getJoueur(copie_jeu) == moi:
        return max([estimation(copie_jeu, c, profondeur - 1) for c in game.getCoupsValides(copie_jeu)])


def evaluation(jeu):
    """ jeu -> float
        Pour un jeu, renvoie l'évaluation de celui-ci
    """
    return 1*difference_score(jeu) + 1*garderGraine(jeu) + 1*piege(jeu) + 1*cases_attaques2(jeu) + 1*cases_012_consecutives(jeu) +\
           1*affame(jeu)


def difference_score(jeu):
    score = jeu[4]
    # print(jeu[4])
    j_courant = moi
    # print("Joueur : {}".format(j_courant))
    if j_courant == 1:
        j_adverse = 2
    else:
        j_adverse = 1
    # print(score[j_courant-1])
    return score[j_courant - 1] - score[j_adverse - 1]


def cases_012_consecutives(jeu):
    plateau = jeu[0]
    j_courant = moi
    cpt_012 = 0
    nb_012_max = 0
    for e in plateau[j_courant - 1]:
        if e == 0 or e == 1 or e == 2:
            cpt_012 += 1
        else:
            if cpt_012 > nb_012_max:
                nb_012_max = cpt_012
            cpt_012 = 0
    return nb_012_max


def affame(jeu):
    plateau = jeu[0]
    j_courant = moi
    if estAffame(plateau, j_courant-1):
        return -5
    return 5


def estAffame(plateau, joueur):
    for e in plateau[joueur - 1]:
        if e != 0:
            return False
    return True

def garderGraine(jeu):
    plateau = jeu[0]
    j_courant = moi
    nb_graines = 0
    for e in plateau[j_courant - 1 ]:
        nb_graines += e
    return nb_graines

def piege(jeu):
    plateau = jeu[0]
    j_courant = moi
    if j_courant == 1:
        j_adverse = 2
    else:
        j_adverse = 1
    cpt_012 = 0
    nb_012_max = 0
    for e in plateau[j_adverse-1]:
        if e == 1 or e == 2:
            cpt_012 += 1
        else:
            if cpt_012 > nb_012_max:
                nb_012_max = cpt_012
            cpt_012 = 0
    return nb_012_max


def nbr_graine(jeu):
    plateau = jeu[0]
    nb_graines = 0

    for e in plateau[0]:
        nb_graines += e

    for e in plateau[1]:
        nb_graines += e

    return nb_graines


def cases_attaques(jeu):
    plateau = jeu[0]
    j_courant = moi
    nb_cases_att = 0

    for i in range(6):
        c = plateau[j_courant-1][i]
        if c >= 6 - i:
            nb_cases_att += 1

    return nb_cases_att

def cases_attaques2(jeu):
    plateau = jeu[0]
    j_adverse = (moi % 2) + 1
    nb_moyen_graines_mangees = 0
    score_d = jeu[4]
    s_adv = score_d[j_adverse - 1]

    jeu_adv = [plateau, j_adverse, jeu[2], jeu[3], score_d]
    liste_coups = game.getCoupsValides(jeu_adv)

    for coup in liste_coups:
        copie_jeu = game.getCopieJeu(jeu_adv)
        game.joueCoup(copie_jeu, coup)
        score_cour = copie_jeu[4]
        s_adv_cour = score_cour[j_adverse - 1]
        nb_moyen_graines_mangees += (s_adv_cour - s_adv) * (1.0/len(liste_coups))

    return nb_moyen_graines_mangees
