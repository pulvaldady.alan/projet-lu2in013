#!/usr/bin/env python
# -*- coding: utf-8 -*-
from copy import deepcopy


def initialisePlateau():
    """
    void -> plateau
    Renvoie le plateau de jeu d'Awele initial
    """
    # l1 : List[nat] #Liste des graines du joueur 1
    l1 = []

    # l2 : List[nat] #Liste des graines du joueur 2
    l2 = []

    # i : int
    for i in range(0, 6):
        l1.append(4)
        l2.append(4)

    return [l1, l2]


def initScore():
    """
    void -> tuple[nat, nat]
    Renvoie le score initial des deux joueurs
    """
    return (0, 0)


def adverse(joueur):
    """ int -> int
        Renvoie le numero du l'adverse d'un joueur
    """
    if joueur == 1:
        return 2
    else:
        return 1


def estAffame(plateau, joueurAdverse):
    """ jeu -> bool
        Indique si l'adversaire est affame
    """
    for e in plateau[joueurAdverse]:
        if e != 0:
            return False

    return True


def listeCoupsValides(jeu):
    """ jeu->List[coup]
        Retourne la liste des coups valides selon l'etat du jeu. Un coup est valide si:
             - La case correspondant au coup appartient bien au joueur courant
             - La case correspondant au coup contient au moins une graine
             - Le coup nourrit l'adversaire si il est affame.
    """
    #coupsValides : List[coup]
    coupsValides = []

    #plateau : plateau
    plateau = jeu[0]

    #jCourant : int
    jCourant = jeu[1]

    #jAdverse : int
    if jCourant == 1:
        jAdverse = 2
    else :
        jAdverse = 1

    tailleP = len(plateau[jCourant-1])
    if estAffame(plateau, jAdverse-1):
        if jCourant == 2:
            for i in range(6):
                if plateau[jCourant-1][i] > tailleP - i:
                    coupsValides.append((jCourant-1, i))
        else: #jCourant est le joueur 1
            for i in range(6):
                if plateau[jCourant - 1][5-i] > tailleP - i:
                    coupsValides.append((jCourant-1, 5-i))
    else:
        for i in range(6):
            if plateau[jCourant-1][i] > 0:
                coupsValides.append((jCourant-1, i))
    return coupsValides




def peutManger(jeu,c):
    """jeu * Pair[nat nat] -> bool
        Retourne vrai si on peut manger le contenu de la case:
            - c'est une case appartenant a l'adversaire du joueur courant
            - La case contient 2 ou 3 graines
    """
    #plateau : plateau
    plateau = jeu[0]

    #jCourant : int
    jCourant = jeu[1]

    ligne, colonne = c

    if jCourant-1 == ligne:
        return False
    else:
        if plateau[ligne][colonne] == 2 or plateau[ligne][colonne] == 3:
            return True
    return False


def finJeu(jeu):
    """ jeu -> bool
        Retourne vrai si c'est la fin du jeu
    """
    # plateau : plateau
    plateau = jeu[0]

    #somme_j1 : int
    somme_j1 = sommeGraines(plateau, 1)
    #somme_j2 : int
    somme_j2 = sommeGraines(plateau, 2)

    if somme_j1 + somme_j2 == 0:
        return True
    #score : tuple[nat, nat]
    score = jeu[4]
    s_j1, s_j2 = score
    if s_j1 > s_j2 + somme_j1 + somme_j2:
        return True
    if s_j2 > s_j1 + somme_j1 + somme_j2:
        return True
    return False



def getGagnant(jeu):
    """
    jeu -> nat
    A partir d'un jeu, designe le vainqueur (joueur 1 ou joueur 2)
    """

    # score : tuple[nat, nat]
    score = jeu[4]
    score_j1, score_j2 = score

    plateau = jeu[0]
    score_j1 += sommeGraines(plateau, 1)
    score_j2 += sommeGraines(plateau, 2)

    if score_j1 > score_j2 :
        return 1
    if score_j2 > score_j1 :
        return 2
    return 0



def sommeGraines(plateau, joueur):
    """
    plateau * int -> int
    Regarde pour un joueur donne la somme des graines qui se trouvent sur sa partie du plateau
    """
    # somme : int
    somme = 0

    for i in range(len(plateau[joueur - 1])):
        somme += plateau[joueur - 1][i]
    return somme


def nextCase(jeu,case,inv=True):
    """jeu*Pair[nat nat]->Pair[nat nat]
        Retourne la prochaine case sur le plateau, dans le sens inverse des aiguilles d'une montre si inv est vrai,
        dans le sens des aiguilles d'une montre sinon
    """

    if not inv:
        ligne, colonne = case
        if ligne == 0 and colonne < 5:
            return 0, colonne+1
        elif colonne == 5 and ligne == 0:
            return 1, 5
        elif ligne == 1 and colonne != 0:
            return 1, colonne-1
        elif ligne == 1 and colonne == 0:
            return 0, 0

    if inv:
        ligne, colonne = case
        if ligne == 0 and colonne != 0:
            return 0, colonne-1
        elif colonne == 5 and ligne == 1:
            return 0, 5
        elif ligne == 1 and colonne < 5:
            return 1, colonne+1
        elif ligne == 0 and colonne == 0:
            return 1, 0

def distribue(jeu,case,nb):
    """ jeu * Pair[nat nat] * nat -> void
    Egraine nb graines (dans le sens inverse des aiguilles d'une montre) en partant de la case suivant la case suivant celle pointee par cellule,
    puis mange ce qu'on a le droit de manger
    Pseudo-code :
        - Distribution des graines dans le sens inverse des aiguilles d'une montre en evitant la case de depart (si nb>=12, on nourrit plusieurs fois les memes cases)
        - Parcours du plateau dans le sens inverse tant qu'on peut manger des graines
    Note: on egraine pas dans la case de depart si on a fait un tour
    Note2: On ne mange rien si le coup affame l'adversaire
    """
    #plateau : plateau
    plateau = jeu[0]

    #jCourant : int
    jCourant = jeu[1]

    #score : tuple[nat, nat]
    score = jeu[4]

    #caseCourante : tuple[nat, nat]
    caseCourante = case

    #n : int
    n = 0

    while n < nb:
        caseCourante = nextCase(jeu, caseCourante)
        if caseCourante == case :
            continue
        else:
            ligne, colonne = caseCourante
            plateau[ligne][colonne] += 1
            n += 1

    #sommeGrainesMangeables : int
    sommeGrainesMangeables = 0

    #listeCasesMangeables : list[tuple[nat, nat]]
    listeCasesMangeables = []

    #jAdverse : int
    jAdverse = adverse(jCourant)

    ligne, colonne = caseCourante
    while ligne == jAdverse - 1 and (plateau[ligne][colonne] == 2 or plateau[ligne][colonne] == 3) :
        listeCasesMangeables.append(caseCourante)
        sommeGrainesMangeables += plateau[ligne][colonne]
        caseCourante = nextCase(jeu, caseCourante, False)
        ligne, colonne = caseCourante

    if sommeGraines(plateau, jAdverse) - sommeGrainesMangeables > 0 :
        for c in listeCasesMangeables:
            ligne, colonne = c
            plateau[ligne][colonne] = 0
            j1, j2 = score
            if jCourant == 1:
                j1 += sommeGrainesMangeables
            else:
                j2 += sommeGrainesMangeables
            jeu[4] = j1, j2


def joueCoup(jeu, coup):
    """coup->void
            Description : Joue un coup
            Hypothese:le coup est valide
            Met tous les champs à jour (sauf coups valides qui est fixée à None)
        """
    case = coup
    ligne,colonne = case
    plateau = jeu[0]
    nb_graines = plateau[ligne][colonne]
    plateau[ligne][colonne] = 0
    distribue(jeu, case, nb_graines)


