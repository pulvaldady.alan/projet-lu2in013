#!/usr/bin/env python
# -*- coding: utf-8 -*-
import awele
import sys

import game

def testCoupsValides() :
    """ void -> void
        Indique si la fonction listeCoupsValides est correcte sur des tests de situations de jeu differentes
    """

    #Test 1 - Plateau random
    plateau = [[0, 0, 3, 2, 0, 0],
               [1, 1, 1, 3, 1, 1]]

    jCourant = 1
    coupsValides = [(0,2), (0,3)]

    jeu = [plateau, jCourant, [], [], (0, 0)]
    assert(awele.listeCoupsValides(jeu) == coupsValides)

    # Test 2 - joueur adverse affame
    plateau = [[0, 0, 0, 0, 0, 0],
               [1, 5, 2, 0, 1, 0]]

    jCourant = 2
    coupsValides = [(1, 1)]

    jeu = [plateau, jCourant, [], [], (0, 0)]
    assert(awele.listeCoupsValides(jeu) == coupsValides)

    # Test 3 - Aucun coups possibles
    plateau = [[0, 1, 2, 1, 0, 4],
               [0, 0, 0, 0, 0, 0]]

    jCourant = 1
    coupsValides = []

    jeu = [plateau, jCourant, [], [], (0, 0)]
    assert(awele.listeCoupsValides(jeu) == coupsValides)
