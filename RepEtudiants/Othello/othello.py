#!/usr/bin/env python
# -*- coding: utf-8 -*-

def initialisePlateau():
    """
    void-> plateau
    Renvoie le plateau de jeu d'Othello initial
    """
    
    #plateau : List[List[nat]]
    plateau = []

    for i in range(0, 8):
        plateau.append([])
        for j in range(0,8):
            plateau[i].append(0)

    plateau[3][3] = 1
    plateau[4][4] = 1
    plateau[3][4] = 2
    plateau[4][3] = 2

    return plateau


def initScore():
    """
    void -> tuple[nat, nat]
    Renvoie le score initial des deux joueurs
    Ici le score initial est (2, 2) car au debut 2 pions de chaque joueurs sont deja positionnes sur le plateau
    """
    return (2,2)


def adverse(joueur):
    """ int -> int
        Renvoie le numero de l'adverse d'un joueur
    """
    if joueur == 1:
        return 2
    else:
        return 1


def trouveEncadrements(jeu,coup,tous=True):
    """jeu*coup*bool->List[Pair[nat nat]]
        Retourne la liste de directions dans lesquelles on trouve un encadrement pour le coup
        si tous est False, on s'arrete au premier trouve
    """
    plateau = jeu[0]
    j_courant = jeu[1]
    j_adv = adverse(j_courant)
    list_direction = list()
    # Si on trouve un pion un adversaire puis un de nos pion c'est un encadrement
    # Si on arrive a la limite de notre tableau en parcourant ce n'est pas un encadrement
    # Si on arrive sur une case vide ce n'est pas un encadrement
    # -1  : Nord , 1 : Sud , 1 : Est, -1 Ouest
    a, b = coup
    for y in range(-1,2):
        for x in range(-1,2):
            encadrer = False
            ligne = a+y
            colonne = b+x
            while True:
                if ligne < 0:
                    pass
                if colonne < 0:
                    pass
                try:
                    if plateau[ligne][colonne] == j_courant and encadrer:
                        list_direction.append((y,x))
                        if tous:
                            return list_direction
                        break
                    if plateau[ligne][colonne] == j_courant and not encadrer:
                        break
                    if plateau[ligne][colonne] == 0:
                        break
                    if plateau[ligne][colonne] == j_adv:
                        encadrer = True
                except IndexError:
                    break
                ligne += y
                colonne += x
    return list_direction


def listeCoupsValides(jeu):
    """ jeu->List[coup]
        Retourne la liste des coups valides selon l'etat du jeu. Un coup est valide si:
             - La case est vide et touche une case ou un pion de l'adversaire est place (assure par la fonction coups)
             - La case permet d'encadrer au moins une liste de pions de l'adversaire
    """
    cp = coups(jeu)
    s = [x for x in cp if len(trouveEncadrements(jeu, x, False))>0]
    return s


def coups(jeu):
    """jeu->List[coup]
        Renvoie une liste de coups pour le jeu tels qu'ils concernent chacun une case vide qui touche une case
        ou un pion de l'adversaire est place
    """
    # plateau : plateau
    plateau = jeu[0]
    # liste_coups : list[coup]
    liste_coups = []

    for i in range(0, len(plateau)):
        for j in range(0, len(plateau[0])):
            # Pour chaque on regarde si elle est vide et s'il existe au moins une case occupee par l'adversaire autour
            if plateau[i][j] == 0 :
                if voisin_adverse(jeu, (i, j)):
                    liste_coups.append((i, j))
    return liste_coups


def voisin_adverse(jeu, case):
    """ jeu * tuple[nat, nat] -> bool
        Indique si dans au moins une des cases voisines se trouve un pion adverse
    """
    plateau = jeu[0]
    nb_lignes = len(plateau)
    nb_colonnes = len(plateau[0])

    j_adverse = adverse(jeu[1])
    ligne, colonne = case
    # print("Pour la case (" + str(ligne) + ", " + str(colonne) +") :")

    # On regarde les cases dans toutes les directions
    for i in range(-1, 2):
        for j in range(-1, 2):
            # Si i et j valent 0, on regarde la case courante -> ce n'est pas utile
            if i != 0 or j != 0 :
                # On verifie tout d'abord que la direction dans laquelle on regarde est une case valide
                if ligne + i >= 0:
                    if ligne + i < nb_lignes:
                        if 0 <= colonne + j:
                            if colonne + j < nb_colonnes:
                                # Des qu'on trouve une case adverse on renvoie True
                                # print("- La case (" + str(ligne + i) + ", " + str(colonne + j) + ")", end=" ")
                                if plateau[ligne+i][colonne+j] == j_adverse:
                                    # print("est une case adverse")
                                    return True
                                # print("n'est pas une case adverse")
    # Si on n'a pas trouve de pion adverse dans aucune direction alors on renvoie False
    return False

def comptePoints(plateau):
    """ plateau -> tuple[nat, nat]
        Renvoie les scores des joueurs par rapport au nombre de pions qu'ils ont sur le plateau
    """
    # score_j1 : int
    score_j1 = 0
    # score_j2 : int
    score_j2 = 0

    for lig in range(len(plateau)):
        for col in range(len(plateau[lig])):
            if plateau[lig][col] == 1:
                score_j1 += 1
            if plateau[lig][col] == 2 :
                score_j2 += 1

    return score_j1, score_j2
    

def finJeu(jeu):
    """ jeu -> bool
        Retourne vrai si c'est la fin du jeu
    """
    # plateau : plateau
    plateau = jeu[0]
    for lig in range(len(plateau)):
        for col in range(len(plateau[lig])):
            # S'il reste au moins une case vide, le jeu n'est pas fini
            if plateau[lig][col] == 0:
                return False
    return True


def getGagnant(jeu):
    """
    jeu -> nat
    A partir d'un jeu, designe le vainqueur (joueur 1 ou joueur 2)
    """
    #score : tuple[nat, nat]
    score = jeu[4]
    score_j1, score_j2 = score

    if score_j1 > score_j2:
        return 1
    if score_j2 > score_j1:
        return 2
    else:
        return 0


def retourner_pion(coup,jeu):
    """coup*jeu -> void
        retourne les pions"""
    plateau = jeu[0]
    j_courant = jeu[1]
    a, b = coup
    point = 0
    liste_pions_a_retourner = trouveEncadrements(jeu,coup,False)
    for direction in liste_pions_a_retourner:
        y, x = direction
        ligne = a+y
        colonne = b+x

        while plateau[ligne][colonne] != j_courant:
            plateau[ligne][colonne] = j_courant
            ligne += y
            colonne += x



def joueCoup(jeu,coup):
    """ jeu*coup -> void
        joue un coup """
    plateau = jeu[0]
    y, x = coup
    retourner_pion(coup,jeu)
    plateau[y][x] = jeu[1]
    jeu[4] = comptePoints(plateau)




