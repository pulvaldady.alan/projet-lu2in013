import sys
import othello
sys.path.append("")
import game
import test
import random
from datetime import datetime
game.game = othello
sys.path.append("./Joueurs")
random.seed(datetime.now())
import Agent as j1
import Agent2 as j2
import alpha_beta as oracle


game.joueur1 = j1
game.joueur2 = j2
N = 50

# Classification
alpha = 1
W = j1.liste_w  # Initialisation des W

for k in range(N):
    partie = game.initialiseJeu()
    nbCoupsJoues = 0
    while nbCoupsJoues < 100 and not game.finJeu(partie):
        j1.moi = 1
        liste_cv = game.getCoupsValides(partie)  # Liste des coups valides
        if liste_cv is None or liste_cv == []:
            break
        coup_oracle = oracle.saisieCoup(partie)  # Coup joue par l'oracle
        etiquette = [-1] * len(liste_cv)

        i = 0
        while i < len(liste_cv):
            if liste_cv[i] == coup_oracle:
                etiquette[i] = 1
                break
            i += 1

        for i in range(len(liste_cv)):
            yi = etiquette[i]
            copie_jeu = game.getCopieJeu(partie)
            game.joueCoup(copie_jeu, liste_cv[i])  # On joue le coup
            eval = j1.evaluation(copie_jeu)  # On l'evalue

            if yi * eval <= 1:
                fonctions_eval = j1.getScore(copie_jeu)
                for j in range(len(W)):
                    W[j] = W[j] + alpha * yi * fonctions_eval[j]
                    j1.setParam(j, W[j])

        game.joueCoup(partie, j1.saisieCoup(partie))
        liste_cv = game.getCoupsValides(partie)  # Liste des coups valides
        if liste_cv is None or liste_cv == []:
            break
        game.joueCoup(partie, j2.saisieCoup(partie))

        nbCoupsJoues += 1
    print("l=",j1.liste_w)
    alpha *= 0.8
