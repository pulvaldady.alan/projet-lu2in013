#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import othello
from copy import deepcopy
import game


def evaluation(jeu, coup):
    # Copie du plateau

    copie_plateau = deepcopy(jeu[0])
    tour = deepcopy(jeu[1])
    liste_coup_possible = deepcopy(jeu[2])
    scores = deepcopy(jeu[4])
    copie_jeu = [copie_plateau,tour,liste_coup_possible,None,scores]

    # Simulation
    othello.joueCoup(copie_jeu, coup)
    score_j1, score_j2 = copie_jeu[4]

    if copie_jeu[1] == 1:
        return score_j1-score_j2
    if copie_jeu[1] == 2:
        return score_j2-score_j1


def estimation(jeu,listes_des_coups_valides):

    liste_coup_eval = list()
    for coup in listes_des_coups_valides:
        tab_coup_eval = [coup, evaluation(jeu,coup)]
        liste_coup_eval.append(tab_coup_eval)
    # Max sur les evals puis on prend le coup associé
    coup_max = liste_coup_eval[0][0]
    eval_max = 0

    for i in range(1,len(liste_coup_eval)):
        if eval_max < liste_coup_eval[i][1]:
            eval_max = liste_coup_eval[i][1]
            coup_max = liste_coup_eval[i][0]

    return coup_max

def saisieCoup(jeu):
    """ jeu -> coup
        Retourne un coup a jouer
    """
    l_coups_valides = deepcopy(jeu[2])
    coup = estimation(jeu,l_coups_valides)
    return coup
