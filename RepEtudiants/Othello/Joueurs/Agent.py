#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import game
import random
import math

moi = None

def saisieCoup(jeu):
    """ jeu -> coup
        Retourne un coup a jouer
    """
    global moi
    moi = game.getJoueur(jeu)
    coup = decision(jeu)
    return coup


def decision(jeu):
    """ jeu -> coup
        Regarde quel coup est le meillieur puis le renvoie
    """
    liste_coup_eval = list()
    liste_coup_max = list()
    for coup in game.getCoupsValides(jeu):
        tab_coup_eval = [coup, estimation(jeu, coup)]
        liste_coup_eval.append(tab_coup_eval)

    # Max sur les evals puis on prend le coup associé

    eval_max = liste_coup_eval[0][1]
    liste_coup_max.append(liste_coup_eval[0][0])

    for i in range(1, len(liste_coup_eval)):
        if eval_max <= liste_coup_eval[i][1]:
            eval_max = liste_coup_eval[i][1]
            liste_coup_max = []
        if eval_max == liste_coup_eval[i][1]:
            liste_coup_max.append(liste_coup_eval[i][0])
    return liste_coup_max[random.randint(0, len(liste_coup_max) - 1)]


def estimation(jeu, coup):
    """ jeu * coup -> float
        Pour un coup donne, evalue la satisfaction obtenue
    """
    # Copier le jeu , jouer le coup, retourner l'application de la fonction eval sur le jeu modif

    # Copie du jeu
    copie_jeu = game.getCopieJeu(jeu)

    # Jouer le coup
    game.joueCoup(copie_jeu, coup)
    return evaluation(copie_jeu)


"""Liste des coefficients en fonction du temps"""
#     return [mobilite_adversaire(jeu), possession_du_centre(jeu), coins_acquis(jeu), difference_score(jeu)]
liste_w = [1,1,1,1,1,1,1,1,1,1,1]

# Modifie dans le vecteur de parametre la valeur
# a l'indice idx par val


def setParam(idx, val):
    # Modifie dans le vecteur de parametre la valeur
    # a l'indice idx par val
    liste_w[idx] = val


def getParam(idx) :
    return liste_w[idx]


def addToParam(idx,val):
    liste_w[idx] += val


def getScore(jeu):
    return [mobilite_adversaire(jeu), possession_du_centre(jeu), coins_accessibles_adv(jeu), difference_score(jeu), mobilite_moi(jeu),
      degre_liberte(jeu), bord(jeu), pions_assures(jeu), parite(jeu), poid(jeu), stabilite(jeu)]

def evaluation(jeu):
    return prodScal(liste_w, getScore(jeu))


def prodScal(v1, v2):
    somme = 0
    for i in range(len(v1)):
        somme += v1[i] * v2[i]
    return somme


def mobilite_moi(jeu):
    return len(game.getCoupsValides(jeu))

def mobilite_adversaire(jeu):
    if jeu[1] == 1:
        j_adverse = 2
    else:
        j_adverse = 1
    jeu_adverse = [jeu[0], j_adverse, jeu[2], jeu[3], jeu[4]]
    return - len(game.getCoupsValides(jeu_adverse))


def possession_du_centre(jeu):
    nb_cases_centre = 0
    plateau = jeu[0]
    j_courant = jeu[1]

    for i in range(3, 5):
        for j in range(3, 5):
            if plateau[i][j] == j_courant:
                nb_cases_centre += 1
    return nb_cases_centre


def coins_acquis(jeu):
    j_courant = jeu[1]
    plateau = jeu[0]
    nb_coins = 0

    for i in [0, 7]:
        for j in [0, 7]:
            if plateau[i][j] == j_courant:
                nb_coins += 1
    return nb_coins


def difference_score(jeu):
    score = jeu[4]
    # print(jeu[4])
    j_courant = moi
    # print("Joueur : {}".format(j_courant))
    if j_courant == 1:
        j_adverse = 2
    else:
        j_adverse = 1
    # print(score[j_courant-1])
    return score[j_courant - 1] - score[j_adverse - 1]


def coins_accessibles_adv(jeu):
    if jeu[1] == 1:
        j_adverse = 2
    else:
        j_adverse = 1
    copie_jeu = game.getCopieJeu(jeu)
    copie_jeu[1] = j_adverse
    coups_valides_adverse = game.getCoupsValides(copie_jeu)
    nb_coins_accessibles = 0
    for i in [0, 7]:
        for j in [0, 7]:
            if (i, j) in coups_valides_adverse:
                nb_coins_accessibles += 1
    return - nb_coins_accessibles

def degre_liberte(jeu):
    degre = 0
    plateau = jeu[0]
    for a in range(0, 7):
        for b in range(0, 7):
            if plateau[a][b] == jeu[1]:
                degre += nb_pions_autour(jeu,(a,b))
    return degre

def nb_pions_autour(jeu, case):
    pions_autour = 0
    ligne, colonne = case
    plateau = jeu[0]
    for i in range(-1, 2):
      for j in range(-1, 2):
        lig2 = ligne + i
        col2 = colonne + j
        if 0 <= lig2 <= 7 and 0 <= col2 <= 7:
            if plateau[lig2][col2] == 0:
              pions_autour += 1
    return pions_autour



def bord(jeu):
    taille_cj = len(jeu[3])
    ligne, colonne = jeu[3][taille_cj - 1]
    if ligne == 0 or ligne == 7:
        return 1
    if colonne == 0 or colonne == 7:
        return 1
    return 0

def directions_strictes(jeu, pos):
    plateau = jeu[0]
    list_direction = list()
    a, b = pos
    for i in range(-1, 2):
        if i == 0:
            continue
        if 0 <= a + i <= len(plateau) - 1:
            list_direction.append((0, i))
        if 0 <= b + i <= len(plateau) - 1:
            list_direction.append((i, 0))

    return list_direction


def pions_assures(jeu):
    plateau = jeu[0]
    j_courant = jeu[1]
    nb_pions_assures = 0

    pions_visites = [[0, 0, 0, 0, 0, 0, 0, 0],
                     [0, 0, 0, 0, 0, 0, 0, 0],
                     [0, 0, 0, 0, 0, 0, 0, 0],
                     [0, 0, 0, 0, 0, 0, 0, 0],
                     [0, 0, 0, 0, 0, 0, 0, 0],
                     [0, 0, 0, 0, 0, 0, 0, 0],
                     [0, 0, 0, 0, 0, 0, 0, 0],
                     [0, 0, 0, 0, 0, 0, 0, 0]]

    # Liste des coins sur le plateau
    coins = [(0, 0), (0, 7), (7, 0), (7, 7)]
    # c : tuple[int, int] #Parcours des coins
    for c in coins:
        lig, col = c
        # On regarde si le coin nous appartient
        if plateau[lig][col] != j_courant:
            continue
        # Le coin est a nous, on a donc un pion assure (si pas deja visite)
        if  pions_visites[lig][col] != 1 :
            nb_pions_assures += 1
            pions_visites[lig][col] = 1

        # On regarde alors dans toutes les directions possibles par rapport au coin
        for dir in directions_strictes(jeu, c):
            lig, col = c
            # Parcours des pions dans la directions souhaitees
            x, y = dir
            taille_max = len(plateau)
            while lig < taille_max - 1 and col < taille_max - 1:
                lig += y
                col += x
                # Si le pion nous appartient et n'a pas deja ete vu alors il est assure,
                if plateau[lig][col] == j_courant and not (pions_visites[lig][col] == 1):
                    nb_pions_assures += 1
                    pions_visites[lig][col] = 1
                # sinon les suivants ne pourrant pas l'etre (ou le sont deja)
                else:
                    break
    return nb_pions_assures

def parite(jeu):
    nombre_de_pionj1, nombre_de_pionj2 = jeu[4]
    if nombre_de_pionj1 > nombre_de_pionj2:
        return (100*nombre_de_pionj1)/(nombre_de_pionj1+nombre_de_pionj2)
    if nombre_de_pionj1 <= nombre_de_pionj2:
        return -(100*nombre_de_pionj2)/(nombre_de_pionj1+nombre_de_pionj2)



def poid(jeu):
    poid_matrice = [[20, -10, 1, 1, 1, 1, -10, 20],
                     [-10, -7, 1, 1, 1, 10, -7, -10],
                     [1, 1, 1, 1, 1, 10, 1, 1],
                     [1, 1, 1, 1, 1, 1, 1, 1],
                     [1, 1, 1, 1, 1, 10, 1, 1],
                     [1, 1, 1, 1, 1, 1, 10, 1],
                     [-10, -7, 1, 1, 1, 1, -7, -10],
                     [20, -10, 1, 1, 1, 1, -10, 20]]
    plateau = jeu[0]
    somme_pionj1 = 0
    somme_pionj2 = 0
    for i in range(0,7):
        for j in range(0,7):
            if plateau[i][j] == 1:
                somme_pionj1 += poid_matrice[i][j]
            elif plateau[i][j] == 2:
                somme_pionj2 += poid_matrice[i][j]
    sommej1j2 = somme_pionj2+somme_pionj1
    if sommej1j2 == 0:
        sommej1j2 += 1
    if jeu[1] == 1:
        return somme_pionj1 / (sommej1j2)
    else:
        return somme_pionj2 / (sommej1j2)

def stabilite(jeu):
    poid_matrice = [[4, -3, 2, 2, 2, 2, -3, 4],
                     [-3, -4, -1, -1, -1, -1, -4, -3],
                     [2, -1, 1, 0, 0, 1, -1, 2],
                     [2, -1, 0, 1, 1, 0, -1, 2],
                     [2, -1, 0, 1, 1, 0, -1, 2],
                     [2, -1, 1, 0, 0, 1, -1, 2],
                     [-3, -4, -1, -1, -1, -1, -4, -3],
                     [4, -3, 2, 2, 2, 2, -3, 4]]
    plateau = jeu[0]
    somme_pionj1 = 0
    somme_pionj2 = 0
    for i in range(0,7):
        for j in range(0,7):
            if plateau[i][j] == 1:
                somme_pionj1 += poid_matrice[i][j]
            elif plateau[i][j] == 2:
                somme_pionj2 += poid_matrice[i][j]
    sommej1j2 = somme_pionj2+somme_pionj1
    if sommej1j2 == 0:
        sommej1j2 += 1
    if jeu[1] == 1:
        return somme_pionj1 / (sommej1j2)
    else:
        return somme_pionj2 / (sommej1j2)
