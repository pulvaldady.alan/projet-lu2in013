#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import game

def saisieCoup(jeu):
    """ jeu -> coup
        Retourne un coup a jouer
    """
    j_courant = jeu[1]

    while True:
        print("Les coups possibles sont  : {}".format(jeu[2]))
        ligne = input("Joueur {} entrer la ligne : ".format(j_courant))
        colonne = input("Joueur {} entrer la colonne : ".format(j_courant))
        coup = ligne, colonne
        if game.coupValide(jeu, coup):
            break
    return coup

