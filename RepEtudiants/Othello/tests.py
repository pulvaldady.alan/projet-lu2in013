#!/usr/bin/env python
# -*- coding: utf-8 -*-

import othello
import sys

def testCoupsValides():
    """ void -> void
        Effectue un jeu de test dans differentes situations de jeu pour la fonction listeCoupsValides
    """

    # Test 1 - Initialisation

    plateau = [[0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 2, 1, 0, 0, 0],
               [0, 0, 0, 1, 2, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0]]

    jCourant = 1
    coupsValides = [(2, 3), (3, 2), (4, 5), (5, 4)]

    jeu = [plateau, jCourant, [], [], (2, 2)]
    assert (othello.listeCoupsValides(jeu) == coupsValides)

    # Test 2 - PLateau aleatoire

    plateau = [[0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 2, 2, 2, 0, 0, 0],
               [0, 0, 2, 2, 2, 0, 0, 0],
               [1, 1, 2, 1, 1, 1, 0, 0],
               [0, 0, 2, 1, 0, 0, 0, 0],
               [0, 0, 2, 0, 0, 0, 0, 0],
               [0, 0, 2, 0, 0, 0, 0, 0]]

    jCourant = 2
    coupsValides = [(3, 0), (3, 5), (4, 6), (5, 0), (5, 4), (5, 5), (5, 6), (6, 3), (6, 4)]

    jeu = [plateau, jCourant, [], [], (6, 10)]
    assert (othello.listeCoupsValides(jeu) == coupsValides)

    # Test 3 - Aucun coups possibles

    plateau = [[1, 2, 1, 1, 1, 1, 1, 1],
               [1, 1, 2, 1, 1, 1, 1, 1],
               [1, 1, 1, 2, 1, 1, 1, 0],
               [2, 2, 1, 1, 2, 1, 1, 0],
               [1, 2, 2, 1, 1, 2, 1, 0],
               [2, 2, 2, 2, 2, 2, 2, 2],
               [2, 2, 2, 2, 2, 2, 1, 2],
               [2, 2, 2, 2, 2, 2, 1, 0]]

    jCourant = 1
    coupsValides = []

    jeu = [plateau, jCourant, [], [], (0, 0)]
    assert (othello.listeCoupsValides(jeu) == coupsValides)

    # Test 4 - Fin de jeu

    plateau = [[1, 2, 2, 1, 2, 2, 2, 2],
               [2, 2, 2, 1, 2, 2, 1, 2],
               [1, 1, 1, 1, 1, 1, 1, 1],
               [2, 1, 1, 2, 1, 1, 2, 1],
               [1, 1, 2, 1, 1, 1, 2, 2],
               [1, 1, 2, 1, 2, 1, 2, 1],
               [2, 2, 2, 1, 2, 1, 1, 2],
               [2, 2, 2, 1, 1, 2, 2, 1]]

    jCourant = 1
    coupsValides = []

    jeu = [plateau, jCourant, [], [], (0, 0)]
    assert (othello.listeCoupsValides(jeu) == coupsValides)


def testComptePoints():
    # Test 1 - Plateau aleatoire

    plateau = [[0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 2, 2, 2, 0, 0, 0],
               [0, 0, 2, 2, 2, 0, 0, 0],
               [1, 1, 2, 1, 1, 1, 0, 0],
               [0, 0, 2, 1, 0, 0, 0, 0],
               [0, 0, 2, 0, 0, 0, 0, 0],
               [0, 0, 2, 0, 0, 0, 0, 0]]

    score = (6, 10)

    assert (othello.comptePoints(plateau) == score)

    # Test 2 - Fin de jeu

    plateau = [[1, 2, 1, 1, 1, 1, 1, 1],
               [1, 1, 2, 1, 1, 1, 1, 1],
               [1, 1, 1, 2, 2, 2, 2, 2],
               [2, 2, 1, 1, 2, 2, 2, 2],
               [1, 2, 2, 1, 1, 2, 2, 2],
               [2, 2, 2, 2, 2, 2, 2, 2],
               [2, 2, 2, 2, 2, 2, 2, 2],
               [2, 2, 2, 2, 2, 2, 2, 2]]

    score = (22, 42)

    assert (othello.comptePoints(plateau) == score)





def testJoueCoup():
    plateauA = [[0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 1, 0, 0, 0],
               [0, 0, 0, 2, 1, 1, 0, 0],
               [0, 0, 0, 2, 1, 0, 0, 0],
               [0, 0, 1, 2, 1, 1, 0, 0],
               [0, 0, 2, 2, 2, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0]]

    plateauB = [[0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 1, 0, 0, 0],
               [0, 0, 0, 2, 1, 1, 0, 0],
               [0, 0, 0, 2, 2, 2, 0, 0],
               [0, 0, 1, 2, 2, 1, 0, 0],
               [0, 0, 2, 2, 2, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0]]

    plateauC = [[0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 2, 1, 1, 0, 0],
                [0, 0, 0, 2, 2, 2, 0, 0],
                [0, 2, 2, 2, 2, 1, 0, 0],
                [0, 0, 2, 2, 2, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0]]

    jeu = [plateauA, 2, None, [(0,2)], (2, 2)]
    coup = (3, 5)
    othello.joueCoup(jeu,coup)
    # game.affiche(jeu)
    assert(plateauA == plateauB)
    othello.joueCoup(jeu, coup)
    othello.joueCoup(jeu,(4,1))
    # game.affiche(jeu)
    assert(plateauA == plateauC)



