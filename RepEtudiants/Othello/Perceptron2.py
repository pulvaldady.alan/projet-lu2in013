import sys
import othello
sys.path.append("")
import game
import test
import random
import math
from datetime import datetime
game.game = othello
sys.path.append("./Joueurs")
random.seed(datetime.now())
import Agent as j1
import Agent2 as j2
import Oracle as oracle

listex = []
listey = []
game.joueur1 = j1
game.joueur2 = j2
N = 20

# Classification
alpha = 2
profondeur = 4
W = j1.liste_w  # Initialisation des W
convergence = False

for k in range(50):
    partie = game.initialiseJeu()
    nbCoupsJoues = 0
    while nbCoupsJoues < 100 and not game.finJeu(partie):
        j1.moi = 1
        liste_cv = game.getCoupsValides(partie)  # Liste des coups valides
        if liste_cv is None or liste_cv == []:
            break
        coup_oracle = oracle.saisieCoup(partie)  # Coup joue par l'oracle
        liste_eval = oracle.liste_coup_eval
        eval_opt_oracle = 0
        for i in range(len(liste_eval)):
            if liste_eval[i][0] == coup_oracle:
                eval_opt_oracle = liste_eval[i][1]
                break


        for i in range(len(liste_cv)):
            eval_coup_oracle = liste_eval[i][1]
            if eval_coup_oracle < eval_opt_oracle:
                # Evaluation du coup courant par le joueur apprenti
                copie_jeu = game.getCopieJeu(partie)
                game.joueCoup(copie_jeu, liste_cv[i])  # On joue le coup
                eval_coup_j1 = j1.evaluation(copie_jeu)  # On l'evalue

                # Evaluation du coup optimal par le joueur apprenti
                copie_jeu2 = game.getCopieJeu(partie)
                game.joueCoup(copie_jeu2, coup_oracle)  # On joue le coup
                eval_opt_j1 = j1.evaluation(copie_jeu2)  # On l'evalue

                # Modification des W
                if (eval_opt_j1 - eval_coup_j1) < 1:
                    fonctions_eval_coup = j1.getScore(copie_jeu)
                    fonctions_eval_opt = j1.getScore(copie_jeu2)
                    for j in range(len(W)):
                        score_coup = fonctions_eval_coup[j]
                        score_opt = fonctions_eval_opt[j]
                        W[j] = W[j] - alpha * (score_coup - score_opt)
                        j1.setParam(j, W[j])

        game.joueCoup(partie, j1.saisieCoup(partie))
        liste_cv = game.getCoupsValides(partie)  # Liste des coups valides
        if liste_cv is None or liste_cv == []:
            break
        game.joueCoup(partie, j2.saisieCoup(partie))

        nbCoupsJoues += 1
    print("L = ", j1.liste_w)
    alpha *= 0.9
