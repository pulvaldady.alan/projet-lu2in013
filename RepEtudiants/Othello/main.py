#!/usr/bin/env python
# -*- coding: utf-8 -*-
import othello
import sys
sys.path.append("..")
import game
game.game=othello
sys.path.append("./Joueurs")
import joueur_humain
import joueur_premier_coup_valide
import joueur_random
import random
import horizon1 as jh1
import min_max as jmm
import alpha_beta as ab

def play_n_game(nbr_parties, joueur_x, joueur_y):
    jx_won_games = 0
    jy_won_games = 0
    egalite = 0
    taux_gain_j1 = 0
    taux_gain_j2 = 0
    taux_partie_nulle = 0
    game.joueur1 = joueur_x
    game.joueur2 = joueur_y
    for i in range(nbr_parties):
        if i == nbr_parties/2:
            game.joueur1 = joueur_y
            game.joueur2 = joueur_x
        # nbCoupsJoues : int
        nbCoupsJoues = 0
        # partie : jeu
        partie = game.initialiseJeu()

        for j in range(4):
            coupsValides = game.getCoupsValides(partie)
            if coupsValides is not None and coupsValides != []:
                indice_coup = random.randint(0, len(coupsValides) - 1)
                game.joueCoup(partie, coupsValides[indice_coup])
                nbCoupsJoues += 1
                continue
            break

        while nbCoupsJoues < 100 or game.finJeu(partie):
            #game.affiche(partie)
            coupsValides = game.getCoupsValides(partie)
            if coupsValides is not None and coupsValides != []:
                coup = game.saisieCoup(partie)
                game.joueCoup(partie, coup)
                nbCoupsJoues += 1
                continue

            break

        jGagnant = game.getGagnant(partie)
        if jGagnant == 0:
            egalite +=1
            print("Egalite")
        elif jGagnant == 1:
            if joueur_x == game.joueur1:
                print("Le Joueur {} a gagne".format(joueur_x))
                jx_won_games += 1
            else:
                print("Le Joueur {} a gagne".format(joueur_y))
                jy_won_games += 1
        else:
            if joueur_x == game.joueur2:
                print("Le Joueur {} a gagne".format(joueur_x))
                jx_won_games += 1
            else:
                print("Le Joueur {} a gagne".format(joueur_y))
                jy_won_games += 1

    taux_gain_jy = jy_won_games / nbr_parties * 100
    taux_gain_jx = jx_won_games / nbr_parties * 100
    taux_partie_nulle = egalite / nbr_parties * 100
    print("Taux d'egalite {}%".format(taux_partie_nulle))
    print("Le joueur {} a gagne {} parties et a un win rate de {}%".format(joueur_x, jx_won_games, taux_gain_jx))
    print("Le joueur {} a gagne {} parties et a un win rate de {}%".format(joueur_y, jy_won_games, taux_gain_jy))

play_n_game(100, ab, jmm)


def play_n_game2(nbr_parties, joueur_x, joueur_y):
    jx_won_games = 0
    jy_won_games = 0
    egalite = 0
    taux_gain_j1 = 0
    taux_gain_j2 = 0
    taux_partie_nulle = 0
    game.joueur1 = joueur_x
    game.joueur2 = joueur_y
    for i in range(nbr_parties):
        if i == nbr_parties/2:
            game.joueur1 = joueur_y
            game.joueur2 = joueur_x
        # nbCoupsJoues : int
        nbCoupsJoues = 0
        # partie : jeu
        partie = game.initialiseJeu()

        for j in range(4):
            coupsValides = game.getCoupsValides(partie)
            if coupsValides is not None and coupsValides != []:
                indice_coup = random.randint(0, len(coupsValides) - 1)
                game.joueCoup(partie, coupsValides[indice_coup])
                nbCoupsJoues += 1
                continue
            break

        while nbCoupsJoues < 100 or game.finJeu(partie):
            #game.affiche(partie)
            coupsValides = game.getCoupsValides(partie)
            if coupsValides is not None and coupsValides != []:
                coup = game.saisieCoup(partie)
                game.joueCoup(partie, coup)
                nbCoupsJoues += 1
                continue

            break

        jGagnant = game.getGagnant(partie)
        if jGagnant == 0:
            egalite +=1
            #print("Egalite")
        elif jGagnant == 1:
            if joueur_x == game.joueur1:
                #print("Le Joueur {} a gagne".format(joueur_x))
                jx_won_games += 1
            else:
                #print("Le Joueur {} a gagne".format(joueur_y))
                jy_won_games += 1
        else:
            if joueur_x == game.joueur2:
                #print("Le Joueur {} a gagne".format(joueur_x))
                jx_won_games += 1
            else:
                #print("Le Joueur {} a gagne".format(joueur_y))
                jy_won_games += 1

    taux_gain_jy = jy_won_games / nbr_parties * 100
    taux_gain_jx = jx_won_games / nbr_parties * 100
    taux_partie_nulle = egalite / nbr_parties * 100
    #print("Taux d'egalite {}%".format(taux_partie_nulle))
    #print("Le joueur {} a gagne {} parties et a un win rate de {}%".format(joueur_x, jx_won_games, taux_gain_jx))
    #print("Le joueur {} a gagne {} parties et a un win rate de {}%".format(joueur_y, jy_won_games, taux_gain_jy))
    return taux_gain_jx, taux_gain_jy

