#!/usr/bin/env python
# -*- coding: utf-8 -*-



def initialisePlateau():
    plateau = list()
    for i in range(0, 6):
        plateau.append([])
        for j in range(0, 7):
            plateau[i].append(0)
    return plateau

def initScore():
    return (0,0)

def verifHorizontale(jeu):
    pionAligne = 0
    pionPositif = [1,2]
    plateau = jeu[0]
    # Horizontalement
    for i in range(0,6):
        pionAligne = 0
        pionPred = plateau[i][0]
        pionAligne += 1
        for j in range(1,7):
            if plateau[i][j] in pionPositif:
                if pionPred == 0:
                    pionPred = plateau[i][j]
                    pionAligne = 1
                elif pionPred == plateau[i][j]:
                    pionAligne += 1
                    pionPred = plateau[i][j]
                else:
                    pionAligne = 1
                    pionPred = plateau[i][j]
            if pionAligne == 4:
                return True, pionPred
    return False, -1


def verifVerticale(jeu):
        pionAligne = 0
        pionPositif = [1, 2]
        plateau = jeu[0]

        for i in range(0, 7):
            pionAligne = 0
            pionPred = plateau[0][i]
            pionAligne += 1
            for j in range(1, 6):
                if plateau[j][i] in pionPositif:
                    if pionPred == 0:
                        pionPred = plateau[j][i]
                        pionAligne = 1
                    elif pionPred == plateau[j][i]:
                        pionAligne += 1
                        pionPred = plateau[j][i]
                    else:
                        pionAligne = 1
                        pionPred = plateau[j][i]
                if pionAligne == 4:
                    return True, pionPred
        return False, -1


def verifDiagonale(jeu):
    pionAligne = 0
    pionPositif = [1, 2]
    plateau = jeu[0]
    i = 0
    # 1ere diag
    diagonale1 = [plateau[0][0],plateau[1][1],plateau[2][2],plateau[3][3],plateau[4][4],plateau[5][5]]
    diagonal2 = [plateau[1][0],plateau[2][1],plateau[3][2],plateau[4][3],plateau[5][4]]
    diagonal3 = [plateau[2][0], plateau[3][1], plateau[4][2], plateau[5][3]]
    diagonal6 = [plateau[0][3], plateau[1][2], plateau[2][1], plateau[3][0]]

    diagonal4 = [plateau[0][1],plateau[1][2],plateau[2][3],plateau[3][4],plateau[4][5],plateau[5][6]]
    diagonal5 = [plateau[0][2],plateau[1][3],plateau[2][4],plateau[3][5],plateau[4][6]]
    # 2 eme diag
    diagonale7 = [plateau[0][6], plateau[1][5], plateau[2][4], plateau[3][3], plateau[4][2],plateau[5][1]]
    diagonal8 = [plateau[0][5], plateau[1][4], plateau[2][3], plateau[3][2], plateau[4][1],plateau[5][0]]
    diagonal9 = [plateau[0][4], plateau[1][3], plateau[2][2], plateau[3][1], plateau[4][0]]
    diagonal10 = [plateau[0][3], plateau[1][4], plateau[2][5], plateau[3][6]]

    diagonal11 = [plateau[1][6], plateau[2][5], plateau[3][4], plateau[4][3], plateau[5][2]]
    diagonal12 = [plateau[2][6], plateau[3][5], plateau[4][4], plateau[5][3]]

    list_diag = [diagonale1,diagonal2,diagonal3,diagonal4,diagonal5,diagonale7,diagonal8,diagonal9
                 ,diagonal11,diagonal12,diagonal6,diagonal10]

    for diagonal in list_diag:
        pionAligne = 0
        pionPred = diagonal[0]
        pionAligne += 1
        for k in range(1,len(diagonal)):

            if diagonal[k] in pionPositif:
                if pionPred == 0:
                    pionPred = diagonal[k]
                    pionAligne = 1
                elif pionPred == diagonal[k]:
                    pionAligne += 1
                    pionPred = diagonal[k]
                else:
                    pionAligne = 1
                    pionPred = diagonal[k]
            if pionAligne == 4:
                return True, pionPred
    return False, -1

def pionAlignes(jeu):
    a,_ = verifVerticale(jeu)
    b,_ = verifHorizontale(jeu)
    c,_ = verifDiagonale(jeu)
    return a or b or c

def listeCoupsValides(jeu):
    listcv = list()
    plateau = jeu[0]
    pionVerif = [1,2]
    for j in range(0,7):
        if plateau[0][j] not in pionVerif:
            if (0,j) not in listcv:
                listcv.append((0,j))
    return listcv


# p = [[0, 0, 0, 1, 0, 0, 0], [0, 0, 0, 0, 1, 0, 0], [0, 0, 0, 0, 0, 1, 0], [0, 0, 0, 0, 0, 1, 1], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0]]
# jeu = [p, 2, None, [(0,2)], (2, 2)]
#print(pionAlignes(jeu))


def finJeu(jeu):
    if pionAlignes(jeu):
        return True
    return False


def getGagnant(jeu):
    a = verifDiagonale(jeu)
    b = verifVerticale(jeu)
    c = verifHorizontale(jeu)
    if a:
        _,jg = a
        return jg
    if b:
        _,jg = a
        return jg
    if c:
        _,jg = a
        return jg

#print(getGagnant(jeu))

def joueCoup(jeu,coup):
    ligne,colonne = coup
    plateau = jeu[0]
    trouver = False
    pionVerif = [1,2]
    if plateau[5][colonne] == 0:
        plateau[5][colonne] = jeu[1]
        return
    i = 0
    while not trouver:
        if plateau[ligne+i][colonne] == 0 and plateau[ligne+i+1][colonne] in pionVerif:
            plateau[ligne+i][colonne] = jeu[1]
            return None
        i += 1