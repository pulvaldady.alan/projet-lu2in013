#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import game
from copy import deepcopy

def saisieCoup(jeu):
    """ jeu -> coup
        Retourne un coup a jouer
    """
    j_courant = deepcopy(jeu[1])

    while True:
        print("Les coups possibles sont  : {}".format(jeu[2]))
        colonne = input("Joueur {} entrer la colonne : ".format(j_courant))
        coup = 0, int(colonne)
        if game.coupValide(jeu, coup):
            break
    return coup
