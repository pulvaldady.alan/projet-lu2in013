#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import game
import random
import math

#l = [-3.417650526771725, 3.7947006135967167, -7.831623710478566, 6.62871287838655]
l = [1,1,1,1]


moi = None


def saisieCoup(jeu):
    """ jeu -> coup
        Retourne un coup a jouer
    """
    global moi
    moi = game.getJoueur(jeu)
    coup = decision(jeu)
    return coup


def decision(jeu):
    """ jeu -> coup
        Regarde quel coup est le meillieur puis le renvoie
    """
    liste_coup_eval = list()
    liste_coup_max = list()
    alpha = -math.inf
    for coup in game.getCoupsValides(jeu):
        tab_coup_eval = [coup, estimation(jeu, coup,3, alpha,  math.inf)]
        alpha = tab_coup_eval[1]
        liste_coup_eval.append(tab_coup_eval)

    # Max sur les evals puis on prend le coup associé

    eval_max = liste_coup_eval[0][1]
    liste_coup_max.append(liste_coup_eval[0][0])

    for i in range(1, len(liste_coup_eval)):
        if eval_max <= liste_coup_eval[i][1]:
            eval_max = liste_coup_eval[i][1]
            liste_coup_max = []
        if eval_max == liste_coup_eval[i][1]:
            liste_coup_max.append(liste_coup_eval[i][0])
    return liste_coup_max[random.randint(0, len(liste_coup_max) - 1)]


def estimation(jeu, coup, profondeur, alpha, beta):
    """ jeu * coup -> float
        Pour un coup donne, evalue la satisfaction obtenue
    """

    # Copier le jeu , jouer le coup, retourner l'application de la fonction eval sur le jeu modif

    # Copie du jeu
    copie_jeu = game.getCopieJeu(jeu)

    # Jouer le coup
    game.joueCoup(copie_jeu, coup)
    # print(game.finJeu(copie_jeu))
    if game.finJeu(copie_jeu):
        g = game.getGagnant(copie_jeu)
        if g == 0:
            return -100
        if g == game.getJoueur(copie_jeu):
            return 1000
        else:
            return -1000
    v = 0
    if profondeur == 1:
        return evaluation(copie_jeu)
    if game.getJoueur(copie_jeu) != moi: # Noeud MIN
        v = math.inf
        for c in game.getCoupsValides(copie_jeu):
            v = min(v, estimation(copie_jeu, c, profondeur - 1, alpha, beta))
            if v <= alpha:
                return v-1
            beta = min(beta, v)
    if game.getJoueur(copie_jeu) == moi: # Noeud MAX
        v = - math.inf
        for c in game.getCoupsValides(copie_jeu):
            v = max(v, estimation(copie_jeu, c, profondeur - 1, alpha, beta))
            if v >= beta:
                return v
            alpha = max(alpha, v)
    return v


def getScore(jeu):
    return [coup_centrale(jeu),alignement(jeu),coup_bord(jeu),bon_plateau(jeu)]


def evaluation(jeu):
    return prodScal(l, getScore(jeu))


def prodScal(v1, v2):
    somme = 0
    for i in range(len(v1)):
        somme += v1[i] * v2[i]
    return somme

def coup_centrale(jeu):
    plateau = jeu[0]
    somme = 0
    for i in range(0,6):
        if plateau[i][2] == moi:
            somme += 100
        else:
            somme -= 100
    return somme

def coup_bord(jeu):
    plateau = jeu[0]
    somme = 0
    for i in range(0,6):
        if plateau[i][0] == moi:
            somme += 50
        else:
            somme -= 50
    for i in range(0,6):
        if plateau[i][6] == moi:
            somme += 50
        else:
            somme -= 50
    return somme

def verifHorizontale(jeu):
    pionAligne = 0
    pionPositif = [1,2]
    plateau = jeu[0]
    # Horizontalement
    for i in range(0,6):
        pionAligne = 0
        pionPred = plateau[i][0]
        pionAligne += 1
        for j in range(1,7):
            if plateau[i][j] in pionPositif:
                if pionPred == 0:
                    pionPred = plateau[i][j]
                    pionAligne = 1
                elif pionPred == plateau[i][j]:
                    pionAligne += 1
                    pionPred = plateau[i][j]
                else:
                    pionAligne = 1
                    pionPred = plateau[i][j]
            if pionAligne == 4:
                return True, pionPred
    return False, -1


def verifVerticale(jeu):
        pionAligne = 0
        pionPositif = [1, 2]
        plateau = jeu[0]

        for i in range(0, 7):
            pionAligne = 0
            pionPred = plateau[0][i]
            pionAligne += 1
            for j in range(1, 6):
                if plateau[j][i] in pionPositif:
                    if pionPred == 0:
                        pionPred = plateau[j][i]
                        pionAligne = 1
                    elif pionPred == plateau[j][i]:
                        pionAligne += 1
                        pionPred = plateau[j][i]
                    else:
                        pionAligne = 1
                        pionPred = plateau[j][i]
                if pionAligne == 4:
                    return True, pionPred
        return False, -1


def verifDiagonale(jeu):
    pionAligne = 0
    pionPositif = [1, 2]
    plateau = jeu[0]
    i = 0
    # 1ere diag
    diagonale1 = [plateau[0][0],plateau[1][1],plateau[2][2],plateau[3][3],plateau[4][4],plateau[5][5]]
    diagonal2 = [plateau[1][0],plateau[2][1],plateau[3][2],plateau[4][3],plateau[5][4]]
    diagonal3 = [plateau[2][0], plateau[3][1], plateau[4][2], plateau[5][3]]
    diagonal6 = [plateau[0][3], plateau[1][2], plateau[2][1], plateau[3][0]]

    diagonal4 = [plateau[0][1],plateau[1][2],plateau[2][3],plateau[3][4],plateau[4][5],plateau[5][6]]
    diagonal5 = [plateau[0][2],plateau[1][3],plateau[2][4],plateau[3][5],plateau[4][6]]
    # 2 eme diag
    diagonale7 = [plateau[0][6], plateau[1][5], plateau[2][4], plateau[3][3], plateau[4][2],plateau[5][1]]
    diagonal8 = [plateau[0][5], plateau[1][4], plateau[2][3], plateau[3][2], plateau[4][1],plateau[5][0]]
    diagonal9 = [plateau[0][4], plateau[1][3], plateau[2][2], plateau[3][1], plateau[4][0]]
    diagonal10 = [plateau[0][3], plateau[1][4], plateau[2][5], plateau[3][6]]

    diagonal11 = [plateau[1][6], plateau[2][5], plateau[3][4], plateau[4][3], plateau[5][2]]
    diagonal12 = [plateau[2][6], plateau[3][5], plateau[4][4], plateau[5][3]]

    list_diag = [diagonale1,diagonal2,diagonal3,diagonal4,diagonal5,diagonale7,diagonal8,diagonal9
                 ,diagonal11,diagonal12,diagonal6,diagonal10]

    for diagonal in list_diag:
        pionAligne = 0
        pionPred = diagonal[0]
        pionAligne += 1
        for k in range(1,len(diagonal)):

            if diagonal[k] in pionPositif:
                if pionPred == 0:
                    pionPred = diagonal[k]
                    pionAligne = 1
                elif pionPred == diagonal[k]:
                    pionAligne += 1
                    pionPred = diagonal[k]
                else:
                    pionAligne = 1
                    pionPred = diagonal[k]
            if pionAligne == 4:
                return True, pionPred
    return False, -1

def alignement(jeu):
    a,m1 = verifDiagonale(jeu)
    b,m2= verifVerticale(jeu)
    c,m3 = verifVerticale(jeu)
    if a:
        if m1 == moi:
            return 1000
        else:
            return -1500
    if b:
        if m2 == moi:
            return 1000
        else:
            return -1500
    if c:
        if m3 == moi:
            return 1000
        else:
            return -1500
    return -15

def bon_plateau(jeu):
    plateau_es = [[4, 10, 8, 51, 2, 49, 5], [-4, 26, 0, 56, 32, 84, 1], [6, 15, 17, 47, 62, 25, 42], [4, 19, 23, 52, 26, 56, -10], [5, 10, 7, 77, 51, 60, 46], [-1, 13, 15, 71, 39, 80, 6]]
    plateau = jeu[0]
    somme = 0
    for i in range(0, 6):
        for j in range(0, 7):
            if plateau[i][j] == moi:
                somme += plateau_es[i][j]
    return somme
