#!/usr/bin/env python
# -*- coding: utf-8 -*-
import puissance4
import sys
sys.path.append("..")
import game
game.game=puissance4
sys.path.append("./Joueurs")
import random
import joueur_humain as jh
import joueur_random as jr
import joueur_premier_coup_valide as jpcv
import alpha_beta_stratN as abs

def play_n_game(nbr_parties, joueur_x, joueur_y):
    jx_won_games = 0
    jy_won_games = 0
    egalite = 0
    taux_gain_j1 = 0
    taux_gain_j2 = 0
    taux_partie_nulle = 0
    game.joueur1 = joueur_x
    game.joueur2 = joueur_y
    for i in range(nbr_parties):
        if i == nbr_parties/2:
            game.joueur1 = joueur_y
            game.joueur2 = joueur_x
        # nbCoupsJoues : int
        nbCoupsJoues = 0
        # partie : jeu
        partie = game.initialiseJeu()

        for j in range(4):
            coupsValides = game.getCoupsValides(partie)
            if coupsValides is not None and coupsValides != []:
                indice_coup = random.randint(0, len(coupsValides) - 1)
                game.joueCoup(partie, coupsValides[indice_coup])
                nbCoupsJoues += 1
                continue
            break

        while nbCoupsJoues < 100 and game.finJeu(partie) == False:
            coupsValides = game.getCoupsValides(partie)
            if coupsValides is not None and coupsValides != []:
                coup = game.saisieCoup(partie)
                game.joueCoup(partie, coup)
                nbCoupsJoues += 1
                continue

            break

        jGagnant = game.getGagnant(partie)
        if jGagnant == 0:
            egalite +=1
            print("Egalite")
        elif jGagnant == 1:
            if joueur_x == game.joueur1:
                print("Le Joueur {} a gagne".format(joueur_x))
                jx_won_games += 1
            else:
                print("Le Joueur {} a gagne".format(joueur_y))
                jy_won_games += 1
        else:
            if joueur_x == game.joueur2:
                print("Le Joueur {} a gagne".format(joueur_x))
                jx_won_games += 1
            else:
                print("Le Joueur {} a gagne".format(joueur_y))
                jy_won_games += 1
    taux_gain_jy = jy_won_games / nbr_parties * 100
    taux_gain_jx = jx_won_games / nbr_parties * 100
    taux_partie_nulle = egalite / nbr_parties * 100
    print("Taux d'egalite {}%".format(taux_partie_nulle))
    print("Le joueur {} a gagne {} parties et a un win rate de {}%".format(joueur_x, jx_won_games, taux_gain_jx))
    print("Le joueur {} a gagne {} parties et a un win rate de {}%".format(joueur_y, jy_won_games, taux_gain_jy))

play_n_game(100,jr,abs)

def play_n_game2(nbr_parties, joueur_x, joueur_y):
    jx_won_games = 0
    jy_won_games = 0
    egalite = 0
    game.joueur1 = joueur_x
    game.joueur2 = joueur_y

    for i in range(nbr_parties):
        if i == nbr_parties / 2:
            game.joueur1 = joueur_y
            game.joueur2 = joueur_x
            # nbCoupsJoues : int
        nbCoupsJoues = 0
        # partie : jeu
        partie = game.initialiseJeu()

        for j in range(4):
            coupsValides = game.getCoupsValides(partie)
            if coupsValides is not None and coupsValides != []:
                indice_coup = random.randint(0, len(coupsValides) - 1)
                game.joueCoup(partie, coupsValides[indice_coup])
                nbCoupsJoues += 1
                continue
            break
        while nbCoupsJoues < 96 and not game.finJeu(partie):
            coupsValides = game.getCoupsValides(partie)
            if coupsValides is not None and coupsValides != []:
                coup = game.saisieCoup(partie)
                game.joueCoup(partie, coup)
                nbCoupsJoues += 1
                # print(nbCoupsJoues)
                continue

            break
        jGagnant = game.getGagnant(partie)
        if jGagnant == 0:
            egalite += 1
            # print("Egalite")
        elif jGagnant == 1:
            if joueur_x == game.joueur1:
                # print("Le Joueur {} a gagne".format(joueur_x))
                jx_won_games += 1

            else:
                # print("Le Joueur {} a gagne".format(joueur_y))
                jy_won_games += 1

        else:
            if joueur_x == game.joueur2:
                # print("Le Joueur {} a gagne".format(joueur_x))
                jx_won_games += 1

            else:
                # print("Le Joueur {} a gagne".format(joueur_y))
                jy_won_games += 1

    taux_gain_jy = jy_won_games / nbr_parties * 100
    taux_gain_jx = jx_won_games / nbr_parties * 100
    taux_partie_nulle = egalite / nbr_parties * 100

    return taux_gain_jx, taux_gain_jy


def play_n_game3(nbr_parties, joueur_x, joueur_y):
    jx_won_games = 0
    jy_won_games = 0
    egalite = 0
    game.joueur1 = joueur_x
    game.joueur2 = joueur_y
    plateau = None
    for i in range(nbr_parties):
        if i == nbr_parties / 2:
            game.joueur1 = joueur_y
            game.joueur2 = joueur_x
            # nbCoupsJoues : int
        nbCoupsJoues = 0
        # partie : jeu
        partie = game.initialiseJeu()

        while nbCoupsJoues < 100 and not game.finJeu(partie):
            coupsValides = game.getCoupsValides(partie)
            if coupsValides is not None and coupsValides != []:
                coup = game.saisieCoup(partie)
                game.joueCoup(partie, coup)
                nbCoupsJoues += 1
                # print(nbCoupsJoues)
                continue

            break
        jGagnant = game.getGagnant(partie)
        if jGagnant == 0:
            egalite += 1
            # print("Egalite")
        elif jGagnant == 1:
            if joueur_x == game.joueur1:
                # print("Le Joueur {} a gagne".format(joueur_x))
                jx_won_games += 1

            else:
                # print("Le Joueur {} a gagne".format(joueur_y))
                jy_won_games += 1

        else:
            if joueur_x == game.joueur2:
                # print("Le Joueur {} a gagne".format(joueur_x))
                jx_won_games += 1

            else:
                # print("Le Joueur {} a gagne".format(joueur_y))
                jy_won_games += 1
        plateau = partie[0]

    taux_gain_jy = jy_won_games / nbr_parties * 100
    taux_gain_jx = jx_won_games / nbr_parties * 100
    taux_partie_nulle = egalite / nbr_parties * 100

    return taux_gain_jx, taux_gain_jy, plateau