#!/usr/bin/env python
# -*- coding: utf-8 -*-
from copy import deepcopy


def initialisePlateau():
    ligne = [0, 0, 0, 0, 0, 0, 0]
    plateau = list()

    for i in range(6):
        plateau.append(deepcopy(ligne))
    return plateau


def initScore():
    return 0, 0


def listeCoupsValides(jeu):
    """ La liste des coups valides correspond a toutes les colonnes qui ne sont pas pleines"""
    plateau = jeu[0]
    liste = list()
    for i in range(7):
        if plateau[0][i] == 0:
            liste.append(i)
    return liste


def directions(case):
    ligne, colonne = case
    liste = list()

    if ligne != 0:
        liste.append((-1, 0))
        if colonne != 0:
            liste.append((-1, -1))
        if colonne != 6:
            liste.append((-1, 1))

    if ligne != 5:
        liste.append((1, 0))
        if colonne != 0:
            liste.append((1, -1))
        if colonne != 6:
            liste.append((1, 1))

    if colonne != 0:
        liste.append((0, -1))

    if colonne != 6:
        liste.append((0, 1))

    return liste


def quatre_pions_alignes(jeu, joueur):
    plateau = jeu[0]
    for ligne in range(6):
        for colonne in range(7):
            if plateau[ligne][colonne] == joueur:
                # print("Point de depart :", "(", ligne, ",", colonne, ")", end = "")
                liste_dir = directions((ligne, colonne))
                for d in liste_dir:
                    cpt = 1
                    dl, dc = d
                    lig = ligne
                    col = colonne

                    while cpt < 4:
                        lig += dl
                        col += dc
                        if lig < 0 or lig > 5:
                            break
                        if col < 0 or col > 6:
                            break
                        # print(cpt, "(", lig, ",", col, ")", end = "")
                        if plateau[lig][col] == joueur:
                            cpt += 1
                        else:
                            break
                    # print("Fin")
                    if cpt == 4:
                        return True

    return False


def finJeu(jeu):
    # print("On teste si le joueur", (jeu[1]%2 + 1) , "a gagne")
    if quatre_pions_alignes(jeu, (jeu[1]%2) + 1):
        # print("C'est le cas")
        sc_j1, sc_j2 = jeu[4]
        if jeu[1] == 1:
            jeu[4] = (0, 1)
        else :
            jeu[4] = (1, 0)
        return True
    # print("Ce n'est pas le cas")
    return False


def getGagnant(jeu):
    score_j1, score_j2 = jeu[4]
    if score_j1 == 1:
        return 1
    if score_j2 == 1:
        return 2
    return 0


def ajoutePion(jeu, coup):
    colonne = coup
    plateau = jeu[0]
    for lig in range(5):
        if plateau[lig + 1][colonne] != 0:
            plateau[lig][colonne] = jeu[1]
            return
    plateau[5][colonne] = jeu[1]


def joueCoup(jeu, coup):
    ajoutePion(jeu, coup)