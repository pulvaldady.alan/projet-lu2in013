#!/usr/bin/env python
# -*- coding: utf-8 -*-
import main
import sys
import puissance4
sys.path.append("")
import game
import random
from datetime import datetime
game.game=puissance4
sys.path.append("./Joueurs")
random.seed(datetime.now())
import Joueurs.agent as ag
import Joueurs.alpha_beta as ab
j1 = ag
j2 = ab


def trainee(N, eps, joueur1, joueur2):
    for w in range(N):  # Fin de la boucle
        for i in range(len(joueur1.param)):
            x = random.random()
            if x > 0.5:
                joueur1.addToParam(i, eps)
            else:
                joueur1.addToParam(i, -eps)
            v1, v2 = main.play_n_game2(N, joueur1, joueur2)
            print("")
            print("Taux gain joueur1 :", v1)
            print("Taux gain joueur2 :", v2)
            if v1 > v2:
                j2.setParam(i, joueur1.getParam(i))
            else:
                j1.setParam(i, joueur2.getParam(i))
        eps *= 0.9
        print("Liste des parametres :", joueur1.param)
    return j1.param

print(trainee(50, 1, j1, j2))