#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import game
from copy import deepcopy

def saisieCoup(jeu):
    """ jeu -> coup
        Retourne un coup a jouer
    """
    j_courant = deepcopy(jeu[1])

    while True:
        print("Les colonnes possibles sont  : {}".format(jeu[2]))
        colonne = input("Joueur {} entrer la case : ".format(j_courant))
        coup = int(colonne)
        if game.coupValide(jeu, coup):
            break
    return coup
