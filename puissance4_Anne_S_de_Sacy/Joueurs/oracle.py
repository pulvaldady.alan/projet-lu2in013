#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import RepEtudiants.game as game
import random
import math

# 0 : adverse_aligne_23_pions
# 1 : moi_aligne_23_pions
# 2 : difference_pions_centre
# 3 : coups_encadres
param = [-1.0, -0.047396825024553224, -12.081033749483344, 0.5415291651726193]


def setParam(idx, val):
    # Modifie dans le vecteur de parametre la valeur
    # a l'indice idx par val
    param[idx] = val


def getParam(idx) :
    return param[idx]


def addToParam(idx,val):
    param[idx] += val


moi = None
liste_coup_eval = None


def saisieCoup(jeu):
    """ jeu -> coup
        Retourne un coup a jouer
    """
    global moi
    moi = game.getJoueur(jeu)
    coup = decision(jeu)
    return coup


def decision(jeu):
    """ jeu -> coup
        Regarde quel coup est le meillieur puis le renvoie
    """
    global liste_coup_eval
    liste_coup_eval = list()
    liste_coup_max = list()
    alpha = -math.inf
    for coup in game.getCoupsValides(jeu):
        tab_coup_eval = [coup, estimation(jeu, coup, 5, alpha,  math.inf)]
        alpha = tab_coup_eval[1]
        liste_coup_eval.append(tab_coup_eval)

    # Max sur les evals puis on prend le coup associé

    eval_max = liste_coup_eval[0][1]
    liste_coup_max.append(liste_coup_eval[0][0])

    for i in range(1, len(liste_coup_eval)):
        if eval_max <= liste_coup_eval[i][1]:
            eval_max = liste_coup_eval[i][1]
            liste_coup_max = []
        if eval_max == liste_coup_eval[i][1]:
            liste_coup_max.append(liste_coup_eval[i][0])
    return liste_coup_max[random.randint(0, len(liste_coup_max) - 1)]


def estimation(jeu, coup, profondeur, alpha, beta):
    """ jeu * coup -> float
        Pour un coup donne, evalue la satisfaction obtenue
    """
    # print("\nProfondeur :", profondeur)
    # print("Coup :", coup, end = " - ")
    # Copier le jeu , jouer le coup, retourner l'application de la fonction eval sur le jeu modif

    # Copie du jeu
    copie_jeu = game.getCopieJeu(jeu)

    # Jouer le coup
    game.joueCoup(copie_jeu, coup)
    # print(game.finJeu(copie_jeu))
    if game.finJeu(copie_jeu):
        g = game.getGagnant(copie_jeu)
        if g == 0:
            # print("Il risque d'y avoir une egalite")
            return -1000
        if g == game.getJoueur(copie_jeu):
            # print("Je risque de gagner")
            return 10000
        else:
            # print("L'autre joueur risque de gagner")
            return -10000
    v = 0
    if profondeur == 1:
        eval = evaluation(copie_jeu)
        # print("Evaluation :", eval, end = "")
        return eval
    if game.getJoueur(copie_jeu) != moi: # Noeud MIN
        v = math.inf
        for c in game.getCoupsValides(copie_jeu):
            v = min(v, estimation(copie_jeu, c, profondeur - 1, alpha, beta))
            if v <= alpha:
                return v-1
            beta = min(beta, v)
    if game.getJoueur(copie_jeu) == moi: # Noeud MAX
        v = - math.inf
        for c in game.getCoupsValides(copie_jeu):
            v = max(v, estimation(copie_jeu, c, profondeur - 1, alpha, beta))
            if v >= beta:
                return v
            alpha = max(alpha, v)
    return v


def getScore(jeu):
    return [adverse_aligne_23_pions(jeu), moi_aligne_23_pions(jeu), difference_pions_centre(jeu), coups_encadres(jeu)]


def evaluation(jeu):
    return prodScal(param, getScore(jeu))


def prodScal(v1, v2):
    somme = 0
    for i in range(len(v1)):
        somme += v1[i] * v2[i]
    return somme


def difference_pions_centre(jeu):
    plateau = jeu[0]
    colonne = 3
    nb_pions_moi = 0
    nb_pions_adv = 0

    for ligne in range(6):
        if plateau[ligne][colonne] == 0:
            break
        if plateau[ligne][colonne] == moi:
            nb_pions_moi += 1
        else:
            nb_pions_adv += 1

    return nb_pions_moi - nb_pions_adv


def peutJouer(jeu, case):
    ligne, colonne = case
    plateau = jeu[0]
    for lig in range(5):
        if plateau[lig + 1][colonne] != 0:
            if lig == ligne:
                return True
    return ligne == 5


def directions(case):
    ligne, colonne = case
    liste = list()

    if ligne != 0:
        liste.append((-1, 0))
        if colonne != 0:
            liste.append((-1, -1))
        if colonne != 6:
            liste.append((-1, 1))

    if ligne != 5:
        liste.append((1, 0))
        if colonne != 0:
            liste.append((1, -1))
        if colonne != 6:
            liste.append((1, 1))

    if colonne != 0:
        liste.append((0, -1))

    if colonne != 6:
        liste.append((0, 1))

    return liste


def adverse_aligne_23_pions(jeu):
    plateau = jeu[0]
    joueur = (moi % 2) + 1
    nb_pions = 0

    for ligne in range(6):
        for colonne in range(7):
            if plateau[ligne][colonne] == joueur:
                # print("Point de depart :", "(", ligne, ",", colonne, ")", end = "")
                liste_dir = directions((ligne, colonne))
                for d in liste_dir:
                    cpt = 1
                    dl, dc = d
                    lig = ligne
                    col = colonne

                    while cpt < 3:
                        lig += dl
                        col += dc
                        if lig < 0 or lig > 5:
                            break
                        if col < 0 or col > 6:
                            break
                        # print(cpt, "(", lig, ",", col, ")", end = "")
                        if plateau[lig][col] == joueur:
                            cpt += 1
                        else:
                            break
                    if cpt == 3:
                        nb_pions += 63


                    if cpt == 2:
                        nb_pions += 1
    return nb_pions


def moi_aligne_23_pions(jeu):
    plateau = jeu[0]
    joueur = moi
    nb_pions = 0

    for ligne in range(6):
        for colonne in range(7):
            if plateau[ligne][colonne] == joueur:
                # print("Point de depart :", "(", ligne, ",", colonne, ")", end = "")
                liste_dir = directions((ligne, colonne))
                for d in liste_dir:
                    cpt = 1
                    dl, dc = d
                    lig = ligne
                    col = colonne

                    while cpt < 3:
                        lig += dl
                        col += dc
                        if lig < 0 or lig > 5:
                            break
                        if col < 0 or col > 6:
                            break
                        # print(cpt, "(", lig, ",", col, ")", end = "")
                        if plateau[lig][col] == joueur:
                            cpt += 1
                        else:
                            break
                    if cpt == 3:
                        nb_pions += 6
                    if cpt == 2:
                        nb_pions += 1
    return nb_pions


def egal(plateau, case, joueur):
    ligne, colonne = case
    if ligne < 0 or colonne < 0 or ligne > 5 or colonne > 6:
        return False
    else:
        return plateau[ligne][colonne] == joueur


def encadre(jeu, case, joueur):
    plateau = jeu[0]
    ligne, colonne = case
    if egal(plateau, (ligne + 1, colonne), joueur) and egal(plateau, (ligne - 1, colonne), joueur):
        return True
    if egal(plateau, (ligne, colonne + 1), joueur) and egal(plateau, (ligne, colonne - 1), joueur):
        return True
    if egal(plateau, (ligne + 1, colonne - 1), joueur) and egal(plateau, (ligne - 1, colonne + 1), joueur):
        return True
    if egal(plateau, (ligne + 1, colonne + 1), joueur) and egal(plateau, (ligne - 1, colonne - 1), joueur):
        return True
    return False


def coups_encadres(jeu):
    plateau = jeu[0]
    joueur_adv = (moi % 2) + 1
    joueur_courant = moi
    nb_encadrements_adv = 0
    nb_encadrements_moi = 0

    for ligne in range(6):
        for colonne in range(7):
            if plateau[ligne][colonne] == 0:
                if encadre(jeu, (ligne, colonne), joueur_adv):
                    nb_encadrements_adv += 1
                if encadre(jeu, (ligne, colonne), joueur_courant):
                    nb_encadrements_moi += 1
    return nb_encadrements_moi - nb_encadrements_adv
